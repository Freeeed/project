import './App.css';
import {Card} from './card';

function App() {
  return (
    <div className="App">
        <div class="divcard">
          <Card value={1} />
          <Card value={2} />
          <Card value={3} />
          <Card value={4} />
          <Card value={5} />
          <Card value={6} />
        </div>
        <div class="divcard">
          <Card value={1} />
          <Card value={2} />
          <Card value={3} />
          <Card value={4} />
          <Card value={5} />
          <Card value={6} />
        </div>
        <div class="divcard">
          <Card value={1} />
          <Card value={2} />
          <Card value={3} />
          <Card value={4} />
          <Card value={5} />
          <Card value={6} />
        </div>
    </div>
  );
}

export default App;
