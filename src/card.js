import { useState } from 'react';
import './App.css';



export function Card(props) {
    const [switcheur, setSwitch] = useState("active");
    
    function handlerSwitch() {
        if (switcheur === "active") {
            setSwitch("inactive");
        }
        else {
            setSwitch("active");
        }
    }

    return (
        <div className={`Card ${switcheur}`} onClick={handlerSwitch}>
            <div>
                <div class="recto">{props.value}</div>
                <div class="verso"></div>
            </div>

        </div>


    )


}